package com.aantonenko.epam.coding.test.ordermatching.command;

import java.util.Optional;

public class ListCommand extends Command {

    public ListCommand() {
        super(CommandType.LIST, Optional.empty());
    }
}

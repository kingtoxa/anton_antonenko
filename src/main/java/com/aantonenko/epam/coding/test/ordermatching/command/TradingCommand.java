package com.aantonenko.epam.coding.test.ordermatching.command;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Optional;

public class TradingCommand extends Command {

    public TradingCommand(CommandType sell, Integer price, Integer newQuantity) {
        super(sell, Optional.of(Pair.of(price, newQuantity)));
    }

    public Integer getPrice() {
        return getParams().get().getLeft();
    }

    public Integer getQuantity() {
        return getParams().get().getRight();
    }

    @Override
    public String toString() {
        return getCommandType() + " " + getPrice() + " " + getQuantity();
    }
}

package com.aantonenko.epam.coding.test.ordermatching;

import com.aantonenko.epam.coding.test.ordermatching.command.Command;
import com.aantonenko.epam.coding.test.ordermatching.command.parser.CommandParser;
import com.aantonenko.epam.coding.test.ordermatching.command.CommandType;
import com.aantonenko.epam.coding.test.ordermatching.command.TradingCommand;
import com.aantonenko.epam.coding.test.ordermatching.trading.TradingProcessor;
import com.jasongoodwin.monads.Try;

import java.io.Console;

public class OrderMatching {

    public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> System.out.println("\nExited!")));

        Console console = System.console();
        TradingProcessor tradingService = new TradingProcessor();

        while (true) {
            String inputLine = console.readLine();
            Try<Command> commandTry = CommandParser.parseCommand(inputLine);

            commandTry
                    .onSuccess((command) -> {
                        if (command.getCommandType() == CommandType.LIST) {
                            System.out.println(tradingService.list());
                        } else {
                            System.out.println(tradingService.processCommand((TradingCommand) command));
                        }
                    })
                    .onFailure((ex) -> System.err.println(ex.getMessage()));
        }
    }
}
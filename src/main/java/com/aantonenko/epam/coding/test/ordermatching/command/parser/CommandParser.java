package com.aantonenko.epam.coding.test.ordermatching.command.parser;

import com.aantonenko.epam.coding.test.ordermatching.command.Command;
import com.aantonenko.epam.coding.test.ordermatching.command.CommandType;
import com.aantonenko.epam.coding.test.ordermatching.command.ListCommand;
import com.aantonenko.epam.coding.test.ordermatching.command.TradingCommand;
import com.aantonenko.epam.coding.test.ordermatching.command.exception.InvalidCommandException;
import com.jasongoodwin.monads.Try;
import org.apache.commons.lang3.StringUtils;

public class CommandParser {

    public static Try<Command> parseCommand(String inputLine) {
        return Try.ofFailable(() -> {
            String[] commandParts = StringUtils.trim(inputLine).split("\\s");
            if (commandParts.length != 1 && commandParts.length != 3)
                throw new InvalidCommandException("Command must have 0 or 2 parameters!");

            final String commandType = commandParts[0];
            final Command parsedCommand;
            switch (commandType.toLowerCase()) {
                case "buy":
                    parsedCommand = buyCommandParser(commandParts);
                    break;
                case "sell":
                    parsedCommand = sellCommandParser(commandParts);
                    break;
                case "list":
                    parsedCommand = new ListCommand();
                    break;
                default:
                    throw new InvalidCommandException("Invalid command " + inputLine);
            }
            return parsedCommand;
        });
    }

    private static Command buyCommandParser(String[] commandParts) {
        return tradingCommandParser(commandParts, CommandType.BUY);
    }

    private static Command sellCommandParser(String[] commandParts) {
        return tradingCommandParser(commandParts, CommandType.SELL);
    }

    private static Command tradingCommandParser(String[] commandParts, CommandType commandType) {
        if (commandParts.length != 3)
            throw new InvalidCommandException(commandType.name() + " command must have 2 parameters!");

        Integer price = Try.ofFailable(() -> Integer.parseInt(commandParts[1])).orElseThrow(() -> new InvalidCommandException(commandType.name() + " command parameters must be integers"));
        Integer quantity = Try.ofFailable(() -> Integer.parseInt(commandParts[2])).orElseThrow(() -> new InvalidCommandException(commandType.name() +  " command parameters must be integers"));

        return new TradingCommand(commandType, price, quantity);
    }
}

package com.aantonenko.epam.coding.test.ordermatching.command.exception;

public class InvalidCommandException extends RuntimeException {
    public InvalidCommandException(String s) {
        super(s);
    }
}

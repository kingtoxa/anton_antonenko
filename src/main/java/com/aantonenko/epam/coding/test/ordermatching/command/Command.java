package com.aantonenko.epam.coding.test.ordermatching.command;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Optional;

public abstract class Command {

    private final CommandType commandType;
    private final Optional<Pair<Integer, Integer>> params;

    public Command(CommandType eventType, Optional<Pair<Integer, Integer>> params) {
        this.commandType = eventType;
        this.params = params;
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public Optional<Pair<Integer, Integer>> getParams() {
        return params;
    }
}

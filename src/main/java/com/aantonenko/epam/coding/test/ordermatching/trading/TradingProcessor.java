package com.aantonenko.epam.coding.test.ordermatching.trading;

import com.aantonenko.epam.coding.test.ordermatching.command.CommandType;
import com.aantonenko.epam.coding.test.ordermatching.command.TradingCommand;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TradingProcessor {

    private final List<TradingCommand> commands;

    public TradingProcessor() {
        commands = new ArrayList<>();
    }

    public String processCommand(TradingCommand command) {
        commands.add(command);
        commands.stream().filter(c -> c.getCommandType() == CommandType.SELL).findFirst().ifPresent((sellCommand) -> {
            int sellCapacity = sellCommand.getPrice() * sellCommand.getQuantity();

            commands.stream()
                    .sorted(Comparator.comparing(commands::indexOf).reversed())
                    .filter(c -> c.getCommandType() == CommandType.BUY && c.getQuantity() * c.getPrice() <= sellCapacity)
                    .findFirst()
                    .ifPresent(buyCommand -> {
                        commands.remove(sellCommand);
                        commands.remove(buyCommand);
                        Integer buyCapacity = buyCommand.getPrice() * buyCommand.getQuantity();
                        Integer tradePrice = buyCommand.getPrice();
                        Integer tradeQuantity = buyCapacity / tradePrice;
                        Integer newSellQuantity = (sellCapacity - buyCapacity) / sellCommand.getPrice();
                        if (newSellQuantity > 0)
                            commands.add(new TradingCommand(CommandType.SELL, sellCommand.getPrice(), newSellQuantity));
                        commands.add(new TradingCommand(CommandType.TRADE, tradePrice, tradeQuantity));
                    });
        });
        return "OK";
    }

    public String list() {
        return commands.stream().map(TradingCommand::toString).collect(Collectors.joining("\n"));
    }
}

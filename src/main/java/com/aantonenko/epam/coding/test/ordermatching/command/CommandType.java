package com.aantonenko.epam.coding.test.ordermatching.command;

public enum CommandType {
    BUY, SELL, TRADE, LIST
}
